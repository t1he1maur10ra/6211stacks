﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace stacks2
{
    class Program
    {
        static int counter1;
        static int counter2;
        static void Main(string[] args)
        {
            Stack stack1 = new Stack();
            Console.WriteLine("Please enter a string with parentheses and i will check if its balanced");
            string input = Console.ReadLine();
            foreach (char c in input)
            {
                stack1.Push(c);
            }

            method1(stack1);
            method2(stack1);

            Console.ReadLine();
        }

        static void method1(Stack stack1)
        {
            counter1 = 0;
            counter2 = 0;

            foreach (char c in stack1)
            {
                if (c == '(')
                {
                    counter1++;
                }
                if (c == ')')
                {
                    counter2++;
                }
            }

            if (counter1 == counter2)
            {
                Console.WriteLine("The parentheses are balanced");
            }
            else
            {
                Console.WriteLine("The parentheses are unbalanced");
            }
        }

        static void method2(Stack stack1)
        {
            counter1 = 0;
            counter2 = 0;
            if(stack1.Contains(')') == true)
            {
                counter1++;
            }
            if (stack1.Contains('(') == true)
            {
                counter2++;
            }

            if (counter1 == counter2)
            {
                Console.WriteLine("The parentheses are balanced");
            }
            else
            {
                Console.WriteLine("The parentheses are unbalanced");
            }
        }
    }
}
