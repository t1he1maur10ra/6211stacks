﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace stacks
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack stack1 = new Stack();//Create an instance of the stack.

            Console.WriteLine("Please enter a string to add to the stack");
            string input = Console.ReadLine();//assign the user input to the input variable.

            foreach(char c in input)//break the string down into its individual characters and input them into the stack 1 character at a time.
            {
                stack1.Push(c);
            }

            foreach(var v in stack1)//print the stack to the screen.
            {
                Console.Write(v);
            }

            stack1.Clear();//Clear out the stack.

            Console.WriteLine();
            firstMethod(stack1);//Demonstrate the ability to pass to another method

            Console.ReadLine();
        }

        static void firstMethod(Stack Stack1)
        {
            Console.WriteLine("Please enter a string to add to the stack");
            string input = Console.ReadLine();//assign the user input to the input variable.

            foreach (char c in input)//break the string down into its individual characters and input them into the stack 1 character at a time.
            {
                Stack1.Push(c);
            }

            foreach (var v in Stack1)//print the stack to the screen.
            {
                Console.Write(v);
            }
        }
    }
}
